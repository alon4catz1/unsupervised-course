import matplotlib.pyplot as plt
from pandas import read_csv
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA
from scipy.stats import multivariate_normal


def min_max(data):
    scale = MinMaxScaler()
    return scale.fit_transform(data)


def z_score(data):
    scale = StandardScaler()
    return scale.fit_transform(data)


def day_part(time):
    return np.mod(time, 60 * 60 * 24)


def pca_and_concatenate(data, percent):
    pca = PCA(n_components=percent)
    data_min_max_pca = pca.fit_transform(data)
    return data_min_max_pca


def p_multivariate_normal(data):
    p = multivariate_normal(mean=np.mean(data, axis=0), cov=np.cov(data.T))
    array_p = p.logpdf(data)
    return array_p


def clean(threshold):
    df = read_csv('creditcard_no_noise.csv')
    df['Time'] = df.apply(lambda q: day_part(q['Time']), axis=1)
    cls = df[['Class']]
    data_min_max = min_max(df.drop(['Time', 'Amount', 'Class'], axis=1))
    data = pca_and_concatenate(data_min_max, 0.9)
    p_pca = p_multivariate_normal(data_min_max)
    time = df["Time"][p_pca < threshold]
    amount = df["Amount"][p_pca < threshold]
    return data[p_pca < threshold], cls[p_pca < threshold], time, amount
