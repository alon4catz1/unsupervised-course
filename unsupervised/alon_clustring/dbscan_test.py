from pandas import DataFrame
from sklearn import metrics
from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN

import numpy as np

from unsupervised.alon_clustring.preprocess import clean

if __name__ == '__main__':

    X, cls, time, amount = clean(threshold=35)
    pca = PCA(n_components=2)
    X = DataFrame(X)
    X = pca.fit_transform(X)

    labels_true = cls["Class"]

    eps_range = np.arange(0.01, 0.1, 0.004)
    neighbors_range = range(5, 17)

    best_score = 0
    best_eps = 0
    best_neighbors = 0

    for eps in eps_range:
        for neighbors in neighbors_range:
            # Compute DBSCAN
            db = DBSCAN(eps=eps, min_samples=neighbors).fit(X)
            core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
            core_samples_mask[db.core_sample_indices_] = True
            labels = db.labels_

            # Number of clusters in labels, ignoring noise if present.
            n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
            n_noise_ = list(labels).count(-1)

            print(f"test for eps={eps}, min_samples={neighbors}")
            print('Estimated number of clusters: %d' % n_clusters_)
            print('Estimated number of noise points: %d' % n_noise_)
            homogeneity = metrics.homogeneity_score(labels_true, labels)
            print("Homogeneity: %0.3f" % homogeneity)
            print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
            print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
            print("Adjusted Rand Index: %0.3f"
                  % metrics.adjusted_rand_score(labels_true, labels))
            print("Adjusted Mutual Information: %0.3f"
                  % metrics.adjusted_mutual_info_score(labels_true, labels))
            silhouette_score = metrics.silhouette_score(X, labels)
            if homogeneity > best_score:
                best_score = homogeneity
                best_eps = eps
                best_neighbors = neighbors
            print("Silhouette Coefficient: %0.3f"
                  % silhouette_score)

            # #############################################################################
            # Plot result
            import matplotlib.pyplot as plt

            # Black removed and is used for noise instead.
            unique_labels = set(labels)
            colors = [plt.cm.Spectral(each)
                      for each in np.linspace(0, 1, len(unique_labels))]
            for k, col in zip(unique_labels, colors):
                if k == -1:
                    # Black used for noise.
                    col = [0, 0, 0, 1]

                class_member_mask = (labels == k)

                xy = X[class_member_mask & core_samples_mask]
                plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                         markeredgecolor='k', markersize=14)

                xy = X[class_member_mask & ~core_samples_mask]
                plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                         markeredgecolor='k', markersize=6)

            plt.title('Estimated number of clusters: %d' % n_clusters_)

    print(f"Best silhouette score: {best_score}, received for eps={best_eps}, min_samples={best_neighbors}")
