from numpy import unique
from pandas import DataFrame
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.decomposition import PCA
from sklearn.mixture import GaussianMixture

from unsupervised.alon_clustring.preprocess import clean

if __name__ == '__main__':
    clusters_range = [2, 3, 4]
    for clusters in clusters_range:
        X, cls, time, amount = clean(threshold=35)
        pca = PCA(n_components=2)
        X = DataFrame(X)
        X = pca.fit_transform(X)

        gmm = GaussianMixture(n_components=clusters)
        labels = gmm.fit_predict(X)
        unique_labels = unique(labels)

        times = []
        classes = []
        amounts = []
        for unique_label in unique_labels:
            times.append(time[labels == unique_label])
            amounts.append(amount[labels == unique_label])
            classes.append(cls[labels == unique_label])

        plt.subplots(1, 3, figsize=(12, 4))

        plt.subplot(1, 3, 1)
        plt.title(f"Time distribution within {clusters} clusters")
        for time in times:
            sns.distplot(time, hist=False)

        plt.subplot(1, 3, 2)
        plt.title(f"Amount distribution within {clusters} clusters")
        for amount in amounts:
            sns.distplot(amount, hist=False)

        plt.subplot(1, 3, 3)
        plt.title(f"Class distribution within {clusters} clusters")
        for cls in classes:
            sns.distplot(cls, hist=False)

        plt.tight_layout(pad=2)
        plt.show()
