import pandas as pd
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
import numpy as np

if __name__ == '__main__':
    df = pd.read_csv('creditcard.csv')
    fraud = df.loc[df['Class'] == 1]
    non_fraud = df.loc[df['Class'] == 0].sample(3000)
    new_df = pd.concat([fraud, non_fraud]).sample(frac=1.).reset_index(drop=True)

    # TNSE
    X_r = TSNE(random_state=100).fit_transform(new_df.drop(['Time', 'Amount', 'Class'], axis=1))
    plt.title("TNSE")
    plt.scatter(X_r[:, 0], X_r[:, 1], c=new_df['Class'])
    plt.show()

    # pca variance
    scale = MinMaxScaler()
    data = scale.fit_transform(df.drop(['Class', 'Time', 'Amount'], axis=1))
    pca = PCA()
    pca.fit(data)
    cumsum = np.cumsum(pca.explained_variance_ratio_)
    plt.title("PCA variance")
    plt.plot(cumsum)
    plt.show()
