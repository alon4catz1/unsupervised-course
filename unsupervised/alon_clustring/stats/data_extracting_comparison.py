from pandas import DataFrame, read_csv
from sklearn.decomposition import PCA
import seaborn as sns
import matplotlib.pyplot as plt

from unsupervised.alon_clustring.preprocess import clean, day_part

if __name__ == '__main__':
    df = read_csv("creditcard.csv")
    X = DataFrame(df)
    cls = X["Class"]
    X["Time"] = X.apply(lambda q: day_part(q['Time']), axis=1)
    time = X["Time"]
    amount = X["Amount"]

    X_clean, cls_clean, time_clean, amount_clean = clean(threshold=40)
    pca = PCA(n_components=2)
    X_clean = DataFrame(X_clean)
    X_clean = pca.fit_transform(X_clean)

    plt.subplots(1, 3, figsize=(12, 4))

    plt.subplot(1, 3, 1)
    sns.distplot(cls_clean, hist=False, label="Extracted Data")
    plt.title("Class distribution")

    plt.subplot(1, 3, 2)
    sns.distplot(time, hist=False, label="Original Data")
    sns.distplot(time_clean, hist=False, label="Extracted Data")
    plt.title("Time distribution")

    plt.subplot(1, 3, 3)
    sns.distplot(amount, hist=False, label="Original Data")
    sns.distplot(amount_clean, hist=False, label="Extracted Data")
    plt.title("Amount distribution")

    plt.tight_layout(pad=2)
    plt.show()
