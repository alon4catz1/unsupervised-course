import preprocessing_data as prd
import isofo_svm_fun as isf
import pandas as pd
import warnings


if __name__ == '__main__':
    warnings.filterwarnings("ignore")
    df_noise = pd.read_csv('creditcard.csv')
    df = pd.read_csv('creditcard_no_noise.csv')
    length = len(df)
    length_noise = len(df_noise)
    df['Time'] = df.apply(lambda q: prd.day_part(q['Time']), axis=1)
    df_noise['Time'] = df_noise.apply(lambda q: prd.day_part(q['Time']), axis=1)
    cls = df[['Class']]
    cls_noise = df_noise[['Class']]
    c = cls == 0
    #PCA to 0.9 variance to v1-v28 and min_max, z_score for time and Amount
    data, data_pca = prd.organize_data(df)
    data_noise, data_pca_noise = prd.organize_data(df_noise)
    # with noisy data
    isf.total_svm_isofo(data_noise, data_pca_noise, cls_noise, length_noise, '_noise','part8')
    # with clean data
    isf.total_svm_isofo(data, data_pca, cls, length, '_no_noise', 'part9')
    # _,_, conf = prd.isofo(data_pca, cls, 400 / float(len(df)))