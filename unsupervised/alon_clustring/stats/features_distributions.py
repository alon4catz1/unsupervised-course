from pandas import read_csv, DataFrame
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import seaborn as sns

from unsupervised.alon_clustring.preprocess import clean

if __name__ == '__main__':
    df = read_csv('../creditcard.csv')
    df = DataFrame(df)

    # data, cls = clean(threshold=42)
    # pca = PCA(n_components=2)
    # data = DataFrame(data)
    # data = pca.fit_transform(data)
    # plt.scatter(data[:, 0], data[:, 1])
    # plt.show()


    def plot_feature_distribution(df1, df2, label1, label2, features):
        i = 0
        sns.set_style('whitegrid')
        plt.figure()
        fig, ax = plt.subplots(5, 6, figsize=(18, 22))

        for feature in features:
            i += 1
            plt.subplot(5, 6, i, label=feature)
            plt.tight_layout(pad=5)
            sns.distplot(df1[feature], hist=False, label=label1)
            sns.distplot(df2[feature], hist=False, label=label2)
            plt.xlabel(feature, fontsize=9)
            locs, labels = plt.xticks()
            plt.tick_params(axis='x', which='major', labelsize=6, pad=-6)
            plt.tick_params(axis='y', which='major', labelsize=6)
        plt.show()


    t0 = df.loc[df['Class'] == 0]
    t1 = df.loc[df['Class'] == 1]
    features = df.columns.values[:-1]
    plot_feature_distribution(t0, t1, '0', '1', features)

    print()
