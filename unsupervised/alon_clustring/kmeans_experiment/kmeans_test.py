from pandas import DataFrame
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.metrics import silhouette_score

from unsupervised.alon_clustring.preprocess import clean


if __name__ == '__main__':
    # Setting the scales for kmeans_experiment's params
    thresholds_range = range(-15, 55, 5)
    pca_components_range = range(2, 10)
    n_clusters_range = range(2, 9)

    results = {}
    for threshold in thresholds_range:
        results[threshold] = {}
        for pca_components in pca_components_range:
            results[threshold][pca_components] = {}

    for threshold in thresholds_range:
        for pca_components in pca_components_range:
            X, labels_true, time, amount = clean(threshold=threshold)
            pca = PCA(n_components=pca_components)
            X = DataFrame(X)
            X = pca.fit_transform(X)

            best_n_clusters = 0
            best_score = 0
            for n_clusters in n_clusters_range:
                kmeans_clusterer = KMeans(n_clusters=n_clusters, n_init=5)
                kmeans_labels = kmeans_clusterer.fit_predict(X)

                kmeans_score = silhouette_score(X, kmeans_labels)

                if kmeans_score > best_score:
                    best_score = kmeans_score
                    best_n_clusters = n_clusters

            print(f"thresold = {threshold}, pca_components = {pca_components}")
            print(f"best score: {best_score}, received for n_clusters: {best_n_clusters}")
            results[threshold][pca_components]["score"] = best_score
            results[threshold][pca_components]["n_clusters"] = best_n_clusters

    with open("kmeans_results", 'w') as f:
        f.write(results.__str__())
    print(results)
