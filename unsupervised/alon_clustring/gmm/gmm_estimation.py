import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from pandas import DataFrame
from sklearn import mixture
from sklearn.decomposition import PCA

from unsupervised.alon_clustring.preprocess import clean


if __name__ == '__main__':
    data, cls, time, amount = clean(threshold=35)
    pca = PCA(n_components=2)
    data = DataFrame(data)
    data = pca.fit_transform(data)

    labels_true = cls

    X_train = data

    n_components = [1, 2, 3, 4, 5, 6, 7, 8]
    score = []
    aic = []
    bic = []

    for n in n_components:
        clf = mixture.GaussianMixture(n_components=n, max_iter=400, n_init=4)
        clf.fit(X_train)

        # display predicted scores by the model as a contour plot
        x = np.linspace(min(X_train[:, 0]), max(X_train[:, 0]))
        y = np.linspace(min(X_train[:, 1]), max(X_train[:, 1]))
        X, Y = np.meshgrid(x, y)
        XX = np.array([X.ravel(), Y.ravel()]).T
        Z = -clf.score_samples(XX)
        Z = Z.reshape(X.shape)

        CS = plt.contour(X, Y, Z, norm=LogNorm(vmin=1.0, vmax=1000.0),
                         levels=np.logspace(0, 3, 15), cmap='RdBu')
        CB = plt.colorbar(CS, shrink=0.8, extend='both')
        plt.scatter(X_train[:, 0], X_train[:, 1], .8)

        plt.title(f"Negative log-likelihood predicted by a GMM with {n} clusters")
        plt.axis('tight')
        aic.append(clf.aic(X_train))
        bic.append(clf.bic(X_train))
        score.append(clf.score(X_train))
        plt.show()

    plt.figure(figsize=(11, 4))

    plt.subplot(131)
    plt.plot(n_components, aic, color="red")
    plt.title("AIC")
    plt.subplot(132)
    plt.plot(n_components, bic, color="red")
    plt.title("BIC")
    plt.subplot(133)
    plt.plot(n_components, score, color="red")
    plt.title("Score")
    plt.show()
