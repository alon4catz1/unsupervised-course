import numpy as np
import matplotlib.pyplot as plt
from pandas import DataFrame
from sklearn import mixture
from sklearn.decomposition import PCA

import itertools

from scipy import linalg
import matplotlib as mpl
from sklearn.metrics import silhouette_score

from unsupervised.alon_clustring.preprocess import clean


if __name__ == '__main__':
    X, labels_true, time, amount = clean(threshold=35)
    pca = PCA(n_components=2)
    X = DataFrame(X)
    X = pca.fit_transform(X)

    color_iter = itertools.cycle(
        ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f'])


    def plot_results(X, Y_, means, covariances, title):
        splot = plt.subplot()
        for i, (mean, covar, color) in enumerate(zip(
                means, covariances, color_iter)):
            v, w = linalg.eigh(covar)
            v = 2. * np.sqrt(2.) * np.sqrt(v)
            u = w[0] / linalg.norm(w[0])
            # as the DP will not use every component it has access to
            # unless it needs it, we shouldn't plot the redundant
            # components.
            if not np.any(Y_ == i):
                continue
            plt.scatter(X[Y_ == i, 0], X[Y_ == i, 1], .8, c=color)

            # Plot an ellipse to show the Gaussian component
            angle = np.arctan(u[1] / u[0])
            angle = 180. * angle / np.pi  # convert to degrees
            ell = mpl.patches.Ellipse(mean, v[0], v[1], 180. + angle, color=color)
            ell.set_clip_box(splot.bbox)
            ell.set_alpha(0.5)
            splot.add_artist(ell)

        plt.xlim(min(X[:, 0]), max(X[:, 0]))
        plt.ylim(min(X[:, 1]), max(X[:, 1]))
        plt.xticks(())
        plt.yticks(())
        plt.title(title)


    n_components = [1, 2, 3, 4, 5, 6, 7, 8]
    score = []
    aic = []
    bic = []

    for n in n_components:
        gmm = mixture.GaussianMixture(n_components=n, max_iter=200, n_init=4)
        gmm.fit(X)
        labels = gmm.predict(X)
        if n > 1:
            score.append(silhouette_score(X, labels))
        else:
            score.append(0)
        aic.append(gmm.aic(X))
        bic.append(gmm.bic(X))
        plot_results(X[:, :2], gmm.predict(X), gmm.means_, gmm.covariances_, f"Gaussian Mixture For {n} Clusters")
        plt.show()

    plt.figure(figsize=(12, 4))

    plt.subplot(131)
    plt.plot(n_components, aic, color="red")
    plt.title("AIC")
    plt.subplot(132)
    plt.plot(n_components, bic, color="red")
    plt.title("BIC")
    plt.subplot(133)
    plt.plot(n_components, score, color="red")
    plt.title("Silhouette Score")
    plt.show()
