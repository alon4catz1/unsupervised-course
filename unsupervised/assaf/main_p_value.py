import pandas as pd
import numpy as np
import multivariate_fun as mvf
import preprocessing_data as prd
from sklearn.utils import shuffle
from scipy.stats import multivariate_normal
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import seaborn as sns


if __name__ == '__main__':
    df = pd.read_csv('creditcard_no_noise.csv')
    trues = len(df[df['Class'] == 1])
    data_min_max = prd.min_max(df.drop(['Time', 'Amount', 'Class'], axis=1))
    time_amount = df[['Time', 'Amount']]
    cls = df[['Class']]
    p = 100
    vec = []
    pca = PCA(n_components=0.9)
    data = pca.fit_transform(data_min_max)
    array = mvf.p_multivariate_normal(data)
    #p value for 1 check
    value, threshold = mvf.f_score_opti_value(cls, trues, data, 1)
    for i in range(p):
        cls1 = shuffle(cls).reset_index()
        cls1 = cls1.drop('index', axis=1)
        F1, dummy = mvf.f_score_opti_value(cls1, trues, data, 1)
        vec.append(F1)
    p_value_1 = np.array(vec)
    vec = []
    print('p value for 1 check - ',len(p_value_1[p_value_1 > value]) / float(100))
    sns.distplot(p_value_1, axlabel="F1 Score")
    plt.savefig('part10.4-p_value_1.png')
    plt.close()
    f_score = 5
    #p value for 2 check
    value = mvf.opti_check_2(data, trues, cls, f_score, array, 0)
    for i in range(p):
        cls1 = shuffle(cls).reset_index()
        cls1 = cls1.drop('index', axis=1)
        F1 = mvf.opti_check_2(data, trues, cls1, f_score, array, 0)
        vec.append(F1)
    p_value_2 = np.array(vec)
    vec = []
    print('p value for 2 check - ',len(p_value_2[p_value_2 > value]) / float(p))
    sns.distplot(p_value_2, axlabel="F1 Score")
    plt.savefig('part11.3-p_value_2.png')
    plt.close()
    #p value only second check
    value = mvf.opti_check_2(data, trues, cls, f_score, array, 0)
    for i in range(p):
        F1 = mvf.opti_check_2(data, trues, cls, f_score, array, 1)
        vec.append(F1)
    p_value_second = np.array(vec)
    print('p value for only second check - ',len(p_value_2[p_value_2 > value]) / float(p))
    sns.distplot(p_value_second, axlabel="F1 Score")
    plt.savefig('part11.4-p_value_second.png')