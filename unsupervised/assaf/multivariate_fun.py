import pandas as pd
from sklearn.decomposition import PCA
import numpy as np
import preprocessing_data as prd
from scipy.stats import multivariate_normal
from sklearn.utils import shuffle


def p_multivariate_normal(data):
    p = multivariate_normal(mean=np.mean(data, axis=0), cov=np.cov(data.T))
    array_p = p.logpdf(data)
    return array_p


def f1_score_opti(array_p_all, cls, trues):
    F1 = []
    a = prd.concat(array_p_all, cls)
    a.columns = ['ans', 'Class']
    mean_1 = np.mean(a[a['Class'] == 1]['ans'])
    mean_0 = np.mean(a[a['Class'] == 0]['ans'])
    delta = round(mean_0 - mean_1)
    precision = []
    recall = []
    threshold = []
    for i in range(100):
        x = mean_0 - (i / float(100)) * delta
        array = array_p_all < x
        p, r = prd.precision_recall(cls, array, trues)
        f1 = prd.f1_fun(p, r)
        precision.append(p)
        recall.append(r)
        F1.append([f1])
        threshold.append([x])
    threshold = np.array(threshold)
    F1_array = np.array(F1)
    precision = np.array(precision)
    recall = np.array(recall)
    return F1_array.reshape(-1,1), precision.reshape(-1,1),recall.reshape(-1,1), threshold.reshape(-1,1)


def total_multi(data_min_max, data_time_amount, cls, mode, trues, path):
    for i in range(9):
        percent = i * 0.1 + 0.1
        pca = PCA(n_components=percent)
        data_pca = pca.fit_transform(data_min_max)
        if len(data_time_amount) != 0:
            data_pca = np.concatenate([data_pca, data_time_amount],axis=1)
        p_pca = p_multivariate_normal(data_pca)
        F1_pca, precision_pca, recall_pca, threshold = f1_score_opti(p_pca, cls, trues)
        if i == 0:
            df_R,df_F11 = prd.score_to_df(precision_pca, recall_pca, threshold, F1_pca, percent)
        else:
            dfR, dfF11 = prd.score_to_df(precision_pca, recall_pca, threshold, F1_pca, percent)
            df_R = pd.concat([df_R, dfR])
            df_F11 = pd.concat([df_F11, dfF11])
        # ax2.plot(T_pca, F_pca, label = 0.2 * i + 0.1)
        # ax1.plot(F1_pca[:,0], F1_pca[:,1],label =0.2 * i + 0.1)
    if len(data_time_amount) != 0:
        data = np.concatenate([data_min_max, data_time_amount], axis=1)
    else:
        data = data_min_max
    p = p_multivariate_normal(data)
    F1, precision, recall, threshold = f1_score_opti(p, cls, trues)
    dfR, dfF11 = prd.score_to_df(precision, recall, threshold, F1, 1)
    df_R = pd.concat([df_R, dfR])
    df_F11 = pd.concat([df_F11, dfF11])
    df_R.to_excel(path + '.1-' + 'df_PR_{mode}.xlsx'.format(mode=mode))
    df_F11.to_excel(path + '.2-' +'df_F1_{mode}.xlsx'.format(mode=mode))
    total = pd.concat([df_R[['precision', 'recall']], df_F11[['F1_score', 'value']]], axis=1)
    a = total[(total['F1_score'] == total['F1_score'].max())].reset_index()
    print(mode, ' - ', a.loc[0]['value'], ' - ', 'f1_score - ', round(a.loc[0]['F1_score'], 3), ' precision - ',
          round(a.loc[0]['precision'], 3), ' recall - ',
          round(a.loc[0]['recall'], 3))


def merge_8_9():
    df = pd.read_excel('part9.1-df_PR_no_noise.xlsx')
    df_v28 = pd.read_excel('part10.1-df_PR_v28.xlsx')

    df = df[(df['value'] == 0.8) | (df['value'] == 0.9)]
    df_v28 = df_v28[(df_v28['value'] == 0.8) | (df_v28['value'] == 0.9)]
    df_v28['value'] = df_v28.apply(lambda q: str(q['value']) + 'v28', axis=1)
    df['value'] = df.apply(lambda q: str(q['value']), axis=1)
    total = pd.concat([df, df_v28])
    total.to_excel('part10.3-merge_0.8_0.9.xlsx')


def train_test(data_9, cls, f_score_value):
    df_total = pd.concat([pd.DataFrame(data_9), cls], axis=1)
    df_0 = df_total[df_total['Class'] == 0].drop('Class', axis=1)
    data_0 = df_0.to_numpy()
    np.random.shuffle(data_0)
    df_1 = df_total[df_total['Class'] == 1].drop('Class', axis=1)
    data_1 = df_1.to_numpy()
    np.random.shuffle(data_1)
    fruads_t = round(0.7 * len(data_1))
    regular_t = round(0.7 * len(data_0))
    train = np.concatenate([data_0[:regular_t], data_1[:fruads_t]])
    train_cls = pd.DataFrame(np.concatenate([np.zeros([regular_t, 1]), np.ones([fruads_t, 1])]), columns=['Class'])
    test_cls = pd.DataFrame(np.concatenate([np.zeros(len(data_0) - regular_t), np.ones(len(data_1) - fruads_t)]),
                            columns=['Class'])
    test = np.concatenate([data_0[regular_t:], data_1[fruads_t:]])
    p = multivariate_normal(mean=np.mean(train, axis=0), cov=np.cov(train.T))
    train_array = p.logpdf(train)
    value, threshold1 = f_score_opti_value(train_cls, fruads_t, train, f_score_value)
    train_check1 = train[train_array < threshold1]
    c = train_cls[train_array < threshold1].reset_index()
    c = c.drop('index', axis=1)
    pca = PCA(n_components=1)
    train_check2 = pca.fit_transform(train_check1)
    df_1 = pd.concat([pd.DataFrame(train_check2, columns=['value']), c], axis=1)
    f1, precision, recall, threshold = opti_d1_value(df_1, fruads_t)
    print('train --- ', 'f1_score - ',
          round(f1, 3), ' precision - ', round(precision, 3), ' recall - ', round(recall, 3))
    test_array = p.logpdf(test)
    test_check1 = test[test_array < threshold1]
    c = test_cls[test_array < threshold1].reset_index()
    c = c.drop('index', axis=1)
    pca = PCA(n_components=1)
    test_check2 = pca.fit_transform(test_check1)
    c_alert = c[test_check2 > threshold]
    precision = c_alert.sum()['Class'] / float(c_alert.count()['Class'])
    recall = c_alert.sum()['Class'] / float(len(data_1) - fruads_t)
    f1_score = 2 * (precision * recall) / (precision + recall)
    print('test --- ', 'f1_score - ', round(f1_score, 3), ' precision - ',
          round(precision, 3), ' recall - ', round(recall, 3))


def f_score_opti_value(cls, trues, data, number):
    array_p_all = p_multivariate_normal(data)
    a = prd.concat(array_p_all, cls)
    a.columns = ['ans', 'Class']
    mean_1 = np.mean(a[a['Class'] == 1]['ans'])
    mean_0 = np.mean(a[a['Class'] == 0]['ans'])
    delta = round(mean_0 - mean_1)
    threshold = 0
    f_max = 0
    for i in range(100):
        x = mean_0 - (i / float(100)) * delta
        array = array_p_all < x
        p, r = prd.precision_recall(cls, array, trues)
        f = prd.f_score(p, r, number)
        if f > f_max:
            f_max = f
            threshold = x
    return f_max, threshold


def opti_d1_value(df, trues):
    mean_regular = df[df['Class'] == 0]['value'].mean()
    mean_fraud = df[df['Class'] == 1]['value'].mean()
    delta = mean_fraud - mean_regular
    max_f = 0
    for i in range(100):
        d_i = i / float(100)
        thresh = d_i * delta + mean_regular
        tp = df[df['value'] > thresh]['Class'].sum()
        total_anomaly_report = df[df['value'] > thresh]['Class'].count()
        recall = tp / float(trues)
        if total_anomaly_report == 0:
            precision = 1
        else:
            precision = tp / float(total_anomaly_report)
        f = prd.f_score(precision, recall, 1)
        if f > max_f:
            threshhold = thresh
            p = precision
            r = recall
            max_f = f
    return max_f, p, r, threshhold


def opti_check_2(data, trues, cls, number, array, to_shuf):
    value, threshold = f_score_opti_value(cls, trues, data, number)
    data_check1 = data[array < threshold]
    c = cls[array < threshold].reset_index()
    if to_shuf == 1:
        c = c.drop('index', axis=1)
        c = shuffle(c)
        c = c.reset_index()
    c = c.drop('index', axis=1)
    pca = PCA(n_components=1)
    data_check2 = pca.fit_transform(data_check1)
    df_1 = pd.concat([pd.DataFrame(data_check2, columns=['value']), c], axis=1)
    value1, dummy1, dummy2, dummy3 = opti_d1_value(df_1, trues)
    return value1