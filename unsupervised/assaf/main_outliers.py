import preprocessing_data as prd
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.covariance import EllipticEnvelope
from sklearn.metrics import f1_score
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.spatial.distance import mahalanobis as mb
from sklearn.preprocessing import minmax_scale
from scipy.stats import multivariate_normal
import scipy.stats as stats


def calculate_critical_value(size, alpha):
    t_dist = stats.t.ppf(1 - alpha / (2 * size), size - 2)
    numerator = (size - 1) * np.sqrt(np.square(t_dist))
    denominator = np.sqrt(size) * np.sqrt(size - 2 + np.square(t_dist))
    critical_value = numerator / denominator
    print("Grubbs Critical Value: {}".format(critical_value))
    return critical_value


if __name__ == '__main__':
    df = pd.read_csv('creditcard.csv')
    length = len(df)
    cls = df[['Class']]
    data = prd.z_score(df[['Time', 'Amount']])
    mean = np.mean(data, axis=0)
    cov = np.cov(data.T)
    f = lambda p: mb(p, mean,cov)
    d = map(f, data)
    vec = np.array(list(d))
    # for alpha = 5%
    G_constant = calculate_critical_value(length, 0.05)
    df_no_noise = df[vec <= G_constant].reset_index()
    df_no_noise = df_no_noise.drop('index', axis=1)
    df_no_noise.to_csv('creditcard_no_noise.csv', index =False)