import pandas as pd
from sklearn.metrics import f1_score, roc_curve
from sklearn.ensemble import IsolationForest
from sklearn.metrics import confusion_matrix
from sklearn.svm import OneClassSVM
import preprocessing_data as prd


def isofo(data, cls, PercFraud, trues):


    isofo = IsolationForest(n_estimators=100, max_features=1.0, max_samples=1.0,
                                behaviour="new", bootstrap=False, random_state=22,
                                contamination=PercFraud)
    y_pred = isofo.fit_predict(data)
    array = y_pred == -1
    precision, recall = prd.precision_recall(cls, array, trues)
    f1_score = prd.f1_fun(precision, recall)
    return [precision, recall], f1_score


def svm(data, cls, PercFraud, trues):
    OneSvm = OneClassSVM(nu = PercFraud)
    y_pred = OneSvm.fit_predict(data)
    array = y_pred == -1
    precision, recall = prd.precision_recall(cls, array, trues)
    f1_score = prd.f1_fun(precision, recall)
    return [precision, recall], f1_score


def opti(data, cls, len, kind, trues):
    PR = []
    F1 = []
    for i in range(15):
        PercFraud = (300 + 100 * i) / float(len)
        if kind == 'one_svm':
            pr, f1 = svm(data, cls, PercFraud, trues)
            PR.append(pr)
            F1.append(f1)
        elif kind == 'isofo':
            pr, f1 = isofo(data, cls, PercFraud, trues)
            PR.append(pr)
            F1.append(f1)
    return PR, F1


def total_svm_isofo(data, data_pca, cls, length, name, part):
    trues = len(cls[cls['Class'] == 1])
    PR_one_svm, F1_one_svm = opti(data, cls, length, 'one_svm', trues)
    PR_one_svm_pca, F1_one_svm_pca = opti(data_pca, cls, length, 'one_svm', trues)
    PR_isofo, F1_isofo = opti(data, cls, length, 'isofo', trues)
    PR_isofo_pca, F1_isofo_pca = opti(data_pca, cls, length, 'isofo', trues)
    PR = prd.to_df(PR_isofo, 'isofo')
    PR = pd.concat(
        [PR, prd.to_df(PR_isofo_pca, 'isofo_pca'), prd.to_df(PR_one_svm, 'one_svm'), prd.to_df(PR_one_svm_pca, 'one_svm_pca')])
    PR.to_excel(part + '.3-' + 'PR_one_svm_isofo{name}.xlsx'.format(name=name))
    F1 = prd.to_df(F1_isofo, 'isofo')
    F1 = pd.concat(
        [F1, prd.to_df(F1_isofo_pca, 'isofo_pca'), prd.to_df(F1_one_svm, 'one_svm'), prd.to_df(F1_one_svm_pca, 'one_svm_pca')])
    F1.to_excel(part + '.4-' + 'F1_one_svm_isofo{name}.xlsx'.format(name=name))
    total = pd.concat([PR, F1], axis=1)
    total.columns = ['precision', 'recall', 'value', 'f1_score', 'value1']
    a = total[(total['f1_score'] == total['f1_score'].max())].reset_index()
    print(name, ' - ', a.loc[0]['value'], ' - ', 'f1_score - ', round(a.loc[0]['f1_score'], 3), ' precision - ',
          round(a.loc[0]['precision'], 3), ' recall - ',
          round(a.loc[0]['recall'], 3))