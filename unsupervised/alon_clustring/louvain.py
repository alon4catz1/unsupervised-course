import numpy as np

from pandas import DataFrame
from scipy.sparse import csr_matrix
from scipy.spatial.distance import squareform, pdist
from sklearn.decomposition import PCA
from sknetwork.clustering import Louvain, modularity

from unsupervised.alon_clustring.preprocess import clean

if __name__ == '__main__':

    X, cls, time, amount = clean(threshold=15)
    pca = PCA(n_components=2)
    X = DataFrame(X)
    X = pca.fit_transform(X)

    labels_true = cls
    X = DataFrame(X)
    D = DataFrame(squareform(pdist(X.iloc[:, 1:])))

    values = []
    for i in range(len(D)):
        for j in range(len(D)):
            if j > i:
                values.append(D[i][j])
    threshold = np.mean(values) * 0.35
    D = (D < threshold)
    for i in range(len(D)):
        D[i][i] = 0
    D = csr_matrix(D)

    louvain = Louvain(engine='python', resolution=1)
    labels = louvain.fit_transform(D)

    unique_labels, counts = np.unique(labels, return_counts=True)
    label_to_count = {unique_labels[i]: counts[i] for i in range(len(unique_labels))}
    max_count = max(counts)

    max_index = list(counts).index(max_count)
    label = unique_labels[max_index]
    # frauds_per_label = {i: list(labels[labels_true == 1]).count(i) for i in unique_labels}

    print(f"labels counts: {label_to_count}")
    print(f"modularity: {modularity(D, labels)}")
    # print(f"frauds per label: {frauds_per_label}")

    # COLORS = ['r', 'b', 'g', 'c', 'm', 'y']
    # graph = nx.from_scipy_sparse_matrix(D)
    # nx.draw(graph, node_color=[COLORS[np.mod(labels[i], len(COLORS))] for i in graph])

    print(unique_labels, counts)
