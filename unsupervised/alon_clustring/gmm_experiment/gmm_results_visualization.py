from typing import Dict, Any

import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    with open("gmm_results", 'r') as results:
        results_matrix: Dict[int, Any] = eval(results.read())

        thresholds = results_matrix.keys()
        pca_components = results_matrix[0].keys()

        scores = []
        for threshold in thresholds:
            h = []
            for pca_component in pca_components:
                h.append(round(results_matrix[threshold][pca_component]["score"], 3))
            scores.append(h)

        fig, ax = plt.subplots()
        im = ax.imshow(scores, cmap="RdBu_r", aspect=len(pca_components) / len(thresholds))

        # We want to show all ticks...
        ax.set_xticks(np.arange(len(pca_components)))
        ax.set_yticks(np.arange(len(thresholds)))
        # ... and label them with the respective list entries
        ax.set_xticklabels(pca_components)
        ax.set_yticklabels(thresholds)

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        for i in range(len(thresholds)):
            for j in range(len(pca_components)):
                text = ax.text(j, i, scores[i][j],
                               ha="center", va="center", fontsize='xx-small')

        ax.set_title("Silhouette score for thresholds/pca_components combinations - GMM")
        fig.tight_layout()
        plt.show()

        scores_with_n_clusters = []
        for threshold in thresholds:
            h = []
            for pca_component in pca_components:
                h.append(results_matrix[threshold][pca_component])
            scores_with_n_clusters.append(h)

        best_score = 0
        best_threshold = None
        best_pca_component = None
        for threshold in thresholds:
            for pca_component in pca_components:
                if results_matrix[threshold][pca_component]["score"] > best_score:
                    best_score = results_matrix[threshold][pca_component]["score"]
                    best_threshold = threshold
                    best_pca_component = pca_component
        print(f"Best silhouette score: {best_score}, threshold={best_threshold}, pca_component={best_pca_component}")
