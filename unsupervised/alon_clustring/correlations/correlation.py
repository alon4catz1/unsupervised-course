from pandas import read_csv, DataFrame, concat
import seaborn as sns
import matplotlib.pyplot as plt

if __name__ == '__main__':
    df = read_csv('../creditcard.csv')
    df = DataFrame(df)

    correlations = df.corr()
    sns.heatmap(correlations, square=True)
    plt.show()

    correlations_ascending = correlations.abs().unstack().sort_values(kind="quicksort").reset_index()
    correlations_ascending = correlations_ascending[
        correlations_ascending['level_0'] != correlations_ascending['level_1']]
    print(correlations_ascending.tail(10))

    df_0 = df[df["Class"] == 0]
    df_1 = df[df["Class"] == 1]
    df_0 = df_0.sample(len(df_1))
    df = concat([df_0, df_1])

    correlations = df.corr()
    sns.heatmap(correlations, square=True)
    plt.show()
